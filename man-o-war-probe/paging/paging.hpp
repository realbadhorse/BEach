#pragma once


#include <cstddef>

#include <ntifs.h>





typedef union _virt_addr_t
{
    void* value;
    struct
    {
        unsigned __int64 offset : 12;
        unsigned __int64 pt_index : 9;
        unsigned __int64 pd_index : 9;
        unsigned __int64 pdpt_index : 9;
        unsigned __int64 pml4_index : 9;
        unsigned __int64 reserved : 16;
    }field;
} virt_addr_t, * pvirt_addr_t;
static_assert(sizeof(virt_addr_t) == sizeof(PVOID), "Size mismatch, only 64-bit supported.");

typedef union _pml4e
{
    unsigned __int64 value;
    struct
    {
        unsigned __int64 present : 1;          // Must be 1, region invalid if 0.
        unsigned __int64 ReadWrite : 1;        // If 0, writes not allowed.
        unsigned __int64 user_supervisor : 1;   // If 0, user-mode accesses not allowed.
        unsigned __int64 PageWriteThrough : 1; // Determines the memory type used to access PDPT.
        unsigned __int64 page_cache : 1; // Determines the memory type used to access PDPT.
        unsigned __int64 accessed : 1;         // If 0, this entry has not been used for translation.
        unsigned __int64 Ignored1 : 1;
        unsigned __int64 large_page : 1;         // Must be 0 for PML4E.
        unsigned __int64 Ignored2 : 4;
        unsigned __int64 pfn : 36; // The page frame number of the PDPT of this PML4E.
        unsigned __int64 Reserved : 4;
        unsigned __int64 Ignored3 : 11;
        unsigned __int64 nx : 1; // If 1, instruction fetches not allowed.
    }field;
} pml4e, * ppml4e;
static_assert(sizeof(pml4e) == sizeof(PVOID), "Size mismatch, only 64-bit supported.");

typedef union _pdpte
{
    unsigned __int64 value;
    struct
    {
        unsigned __int64 present : 1;          // Must be 1, region invalid if 0.
        unsigned __int64 rw : 1;        // If 0, writes not allowed.
        unsigned __int64 user_supervisor : 1;   // If 0, user-mode accesses not allowed.
        unsigned __int64 PageWriteThrough : 1; // Determines the memory type used to access PD.
        unsigned __int64 page_cache : 1; // Determines the memory type used to access PD.
        unsigned __int64 accessed : 1;         // If 0, this entry has not been used for translation.
        unsigned __int64 Ignored1 : 1;
        unsigned __int64 large_page : 1;         // If 1, this entry maps a 1GB page.
        unsigned __int64 Ignored2 : 4;
        unsigned __int64 pfn : 36; // The page frame number of the PD of this PDPTE.
        unsigned __int64 Reserved : 4;
        unsigned __int64 Ignored3 : 11;
        unsigned __int64 nx : 1; // If 1, instruction fetches not allowed.
    }field;
} pdpte, * ppdpte;
static_assert(sizeof(pdpte) == sizeof(PVOID), "Size mismatch, only 64-bit supported.");

typedef union _pde
{
    unsigned __int64 value;
    struct
    {
       unsigned __int64 present : 1;          // Must be 1, region invalid if 0.
        unsigned __int64 rw : 1;        // If 0, writes not allowed.
        unsigned __int64 user_supervisor : 1;   // If 0, user-mode accesses not allowed.
        unsigned __int64 PageWriteThrough : 1; // Determines the memory type used to access PT.
        unsigned __int64 page_cache : 1; // Determines the memory type used to access PT.
        unsigned __int64 accessed : 1;         // If 0, this entry has not been used for translation.
        unsigned __int64 Ignored1 : 1;
        unsigned __int64 large_page : 1; // If 1, this entry maps a 2MB page.
        unsigned __int64 Ignored2 : 4;
        unsigned __int64 pfn : 36; // The page frame number of the PT of this PDE.
        unsigned __int64 Reserved : 4;
        unsigned __int64 Ignored3 : 11;
        unsigned __int64 nx : 1; // If 1, instruction fetches not allowed.
    }field;
} pde, * ppde;
static_assert(sizeof(pde) == sizeof(PVOID), "Size mismatch, only 64-bit supported.");

typedef union _pte
{
    unsigned __int64 value;
    struct
    {
        unsigned __int64 present : 1;          // Must be 1, region invalid if 0.
        unsigned __int64 rw : 1;        // If 0, writes not allowed.
        unsigned __int64 user_supervisor : 1;   // If 0, user-mode accesses not allowed.
        unsigned __int64 PageWriteThrough : 1; // Determines the memory type used to access the memory.
        unsigned __int64 page_cache : 1; // Determines the memory type used to access the memory.
        unsigned __int64 accessed : 1;         // If 0, this entry has not been used for translation.
        unsigned __int64 Dirty : 1;            // If 0, the memory backing this page has not been written to.
        unsigned __int64 PageAccessType : 1;   // Determines the memory type used to access the memory.
        unsigned __int64 Global : 1;           // If 1 and the PGE bit of CR4 is set, translations are global.
        unsigned __int64 Ignored2 : 3;
        unsigned __int64 pfn : 36; // The page frame number of the backing physical page.
        unsigned __int64 reserved : 4;
        unsigned __int64 Ignored3 : 7;
        unsigned __int64 ProtectionKey : 4;  // If the PKE bit of CR4 is set, determines the protection key.
        unsigned __int64 nx : 1; // If 1, instruction fetches not allowed.
    }field;
} pte, * ppte;
static_assert(sizeof(pte) == sizeof(PVOID), "Size mismatch, only 64-bit supported.");




PHYSICAL_ADDRESS get_pa_from_va(PVOID paging_base, PVOID _va);
